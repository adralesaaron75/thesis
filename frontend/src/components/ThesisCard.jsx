import React, { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import { FaHeart } from "react-icons/fa";
import StarRating from "./StarRating";
import HeartReacting from "./Heart";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";

const ThesisCard = ({
  date,
  title,
  content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod ex eligendi corporis quo nisi quibusdam mollitia reprehenderit optioLorem ipsum dolor sit amet consectetur adipisicing elit. Quod ex eligendi corporis quo nisi quibusdam mollitia reprehenderit optio dLorem ipsum dolor sit amet consectetur adipisicing elit. Quod ex eligendi corporis quo nisi quibusdam mollitia reprehenderit optio d delectus amet similique nulla voluptates, totam, distinctio enim dolores quisquam excepturi reiciendis.",
  categories,
}) => {
  const [expanded, setExpanded] = useState(false);
  const [truncatedTitle, setTruncatedTitle] = useState("");
  const [truncatedContent, setTruncatedContent] = useState(content);

  useEffect(() => {
    if (title.length > 10) {
      setTruncatedTitle(title.substring(0, 10) + "...");
    } else {
      setTruncatedTitle(title);
    }
  }, [title]);

  const handleArrowClick = () => {
    setExpanded(!expanded);
  };

  useEffect(() => {
    if (expanded) {
      setTruncatedContent(content);
    } else {
      if (content.length > 200) {
        setTruncatedContent(content.substring(0, 200) + "...");
      } else {
        setTruncatedContent(content);
      }
    }
  }, [expanded, content]);

  return (
    <div
      style={{
        padding: "10px",
        width: "70vw",
        height: expanded ? "auto" : "auto",
        background: "White",
        filter: "drop-shadow(0 9px 8px rgba(0, 0, 0, 0.15))",
        marginTop: "30px",
      }}
      className="d-flex justify-content-center"
    >
      <Row>
        <Col>{expanded ? <StarRating /> : <HeartReacting />}</Col>

        <Col className="d-flex justify-content-end">
          {expanded ? (
            <div className="p-2">
              <div style={{ display: "flex", alignItems: "center" }}>
                {categories.map((category, index) => (
                  <p
                    key={index}
                    style={{
                      display: "inline-block",
                      margin: "3px",
                      background: "blue",
                      color: "white",
                      padding: "3px",
                      borderRadius: "5px",
                      fontSize: "10px",
                    }}
                  >
                    {category}
                  </p>
                ))}
                <p style={{ marginLeft: "15px" }}>{date}</p>
              </div>
            </div>
          ) : (
            <p>{date}</p>
          )}
        </Col>

        <div style={{ height: expanded ? "auto" : "auto" }}>
          {expanded ? (
            <h3 style={{ marginLeft: "30px" }} onClick={handleArrowClick}>
              <HeartReacting /> {title}
            </h3>
          ) : (
            <h3 style={{ marginLeft: "30px" }}>{truncatedTitle}</h3>
          )}
          <p
            style={{
              marginLeft: "50px",
              textAlign: "center",
              cursor: "pointer",
            }}
            onClick={handleArrowClick}
          >
            {truncatedContent}
          </p>

          {expanded ? (
            <hr
            class="d-flex justify-content-start"
            style={{
              width: "auto", // Set width to auto to allow it to start from the left
              borderTop: "2px solid #000000",
              margin: "0",
              marginTop: "10px",
              marginBottom: "10px",
              filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.99))'
            }}/>):null}

        </div>

        {expanded ? null : (
          <div style={{ display: "flex", alignItems: "Start" }}>
            {categories.map((category, index) => (
              <p
                key={index}
                style={{
                  display: "inline-block",
                  margin: "5px",
                  background: "blue",
                  color: "white",
                  padding: "5px",
                  borderRadius: "5px",
                  fontSize: "13px",
                }}
              >
                {category}
              </p>
            ))}
            <div style={{ marginLeft: "auto" }}>
              <StarRating />
            </div>
          </div>
        )}
        <footer style={{ marginTop: "auto" }}>
          <div
            className="d-flex justify-content-center "
            onClick={handleArrowClick}
          >
            {expanded ? (
              <IoIosArrowUp style={{ cursor: "pointer" }} />
            ) : (
              <IoIosArrowDown style={{ cursor: "pointer" }} />
            )}
          </div>
        </footer>
        
      </Row>
    </div>
  );
};

export default ThesisCard;
