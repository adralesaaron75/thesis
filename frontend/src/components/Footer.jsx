import React from "react";
import footerssu from "./assets/images/footerSSu.png";


function Footer() {
  return (
    <> <img src={footerssu} alt="Logo" style={{width:"100%", marginTop:'5%'}}></img>
    <footer style={{ background: "maroon", padding: "20px", color: "white", textAlign: "center" }}>
      
      <p>&copy; 2024-All Rights Reserved.</p>
     
    </footer>
    </>
  );
  
}

export default Footer;
