import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import logo from "./assets/images/2072841.png";
import { CDBBox, CDBContainer } from "cdbreact";
import { Link } from "react-router-dom";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import { NavLink } from "react-router-dom";
import AdminDashboard from "./adminside/AdminDashboard";
import SearchForm from "./SearchForm";
import React, { useState } from 'react';
import SidebarLayout from "./adminside/SidebarLayout";

export default function AdminHome({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };


  
  return (
    <div
      style={{ display: "flex", height: "100vh", overflow: "scroll initial" }}
    >
      <CDBBox display="flex" alignContent="start">
        <CDBSidebar textColor="#fff" backgroundColor="#800000">
          <SidebarLayout/>
        </CDBSidebar>
      </CDBBox>


      <CDBBox
        
        display="flex"
        justifyContent="start"
        style={{
          width: "100%",
          height: "100%",
        }}
      >
        
          <AdminDashboard/>

          {/* <SearchForm/> */}



      </CDBBox>
    </div>
  );
}
