import Button from "react-bootstrap/Button";
import logo from "../assets/images/2072841.png";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import { toast } from "react-toastify";
import { CDBBtn, CDBBox, CDBCard, CDBCardBody, CDBContainer } from "cdbreact";
import { Link, useParams } from "react-router-dom";
import Table from "react-bootstrap/Table";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import { NavLink } from "react-router-dom";
import axios from "axios";
import { VITE_BACKEND_URL } from "../../App";
import SidebarLayout from "./SidebarLayout";

export default function Books({ userType }) {
  const [data, setData] = useState([]);
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  const [titlename, setTitlename] = useState("");
  const [booknumbercode, setBooknumbercode] = useState("");
  const [ISBNNumber, setISBNNumber] = useState("");
  const [abstractname, setAuthorname] = useState("");
  const [publishername, setPublishername] = useState("");
  const [publisheddate, setPublisheddate] = useState("");
  const [quantity, setQuantity] = useState("");

  // const [image, setImage] = useState(""); // Excluded based on your request

  // Convert to base64 format
  function covertToBase64(e) {
    console.log(e);
    var reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = () => {
      console.log(reader.result); // Base64encoded string
      // setImage(reader.result); // Excluded based on your request
    };
    reader.onerror = (error) => {
      console.log("Error: ", error);
    };
  }
  // End convert to base64 format

  // Add book
  const handleSubmit = async (e) => {
    if (userType === "admin") {
      e.preventDefault();
      alert("You are not Admin");
    } else {
      e.preventDefault();
      await fetch(`${VITE_BACKEND_URL}/api/add-book`, {
        method: "POST",
        crossDomain: true,
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          titlename,
          booknumbercode,
          ISBNNumber,
          abstractname,
          publishername,
          publisheddate,
          quantity,
          // base64: image, // Excluded based on your request
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data, "Admin-added");
          if (data.status === "ok") {
            toast.success("Add book successfully");
            setTimeout(() => {
              window.location.href = "/admin/books";
            }, 2000);
            getAllBooks();
          } else {
            toast.error("Something went wrong");
          }
        });
    }
  };
  // End add book

  // Fetch all books
  useEffect(() => {
    getAllBooks();
  }, []);

  // Fetching all books
  const getAllBooks = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "bookData");
        setData(data.data);
      });
  };

  // End fetch all books

  // Add modal
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  // End add modal

  // Edit modal
  const [showedit, setShowedit] = useState(false);
  const handleCloseedit = () => setShowedit(false);
  const handleShowedit = () => setShowedit(true);
  // End edit modal


  const handleDropdownSelect = (value) => {
    setPublishername(value);
  };

  return (
    <div
      style={{ display: "flex", height: "100vh", overflow: "scroll initial" }}
    >
      {/* For Sidebar */}
      <CDBBox display="flex" alignContent="start">
        <CDBSidebar textColor="#fff" backgroundColor="#800000">
          <SidebarLayout />
        </CDBSidebar>
      </CDBBox>

      <CDBBox
        m="5"
        display="flex"
        justifyContent="center"
        style={{
          width: "100rem",
          height: "200px",
        }}
      >
        <>
          <CDBContainer>
            <CDBCard>
              <CDBBtn
                color="success"
                onClick={handleShow}
                className="my-2"
                style={{ marginLeft: "10px" }}
              >
                Add Manual
              </CDBBtn>
              <CDBCardBody>
                <Table className="table table-bordered">
                  <thead>
                    <tr>
                      <td>1</td>
                      {/* Image */}
                      <th>2</th>
                      {/* Book Name */}
                      <th>3</th>
                      {/*Book Number  */}
                      <th>4</th>
                      {/* ISBN Number */}
                      <th>5</th>
                      {/* Author Name */}
                      <th>6</th>
                      {/* Publisher Name */}
                      <th>7</th>
                      {/* Published Date */}
                      <th>8</th>
                      {/* Quantity */}
                      <th>9</th>
                      {/* Action */}
                    </tr>
                  </thead>
                  <tbody>
                    {data.map((i, index) => {
                      return (
                        <tr key={index}>
                          <td>
                            {i.image == "" || i.image == null ? (
                              <img
                                width={10}
                                height={0}
                                src="https://st.depositphotos.com/2934765/53192/v/450/depositphotos_531920820-stock-illustration-photo-available-vector-icon-default.jpg"
                                alt="default image"
                                style={{ width: "60", height: "60" }}
                              />
                            ) : (
                              <img width={60} height={60} src={i.image} />
                            )}
                          </td>
                          <td>{i.titlename}</td>
                          <td>{i.booknumbercode}</td>
                          <td>{i.ISBNNumber}</td>
                          <td>{i.abstractname}</td>
                          <td>{i.publishername}</td>
                          <td>{i.publisheddate}</td>
                          <td>{i.quantity}</td>
                          <td>
                            <div className="d-flex">
                              <div className="flex-1">
                                <CDBBtn
                                  color="primary"
                                  onClick={() =>
                                    getSinglBook(
                                      i._id,
                                      i.titlename,
                                      i.booknumbercode,
                                      i.ISBNNumber,
                                      i.abstractname,
                                      i.publishername,
                                      i.publisheddate,
                                      i.quantity
                                    )
                                  }
                                >
                                  <span onClick={handleShowedit}>Edit</span>
                                </CDBBtn>
                              </div>
                              &nbsp;
                              <div className="flex-1">
                                <CDBBtn
                                  color="danger"
                                  onClick={() => deleteBook(i._id, i.titlename)}
                                >
                                  Delete
                                </CDBBtn>
                              </div>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              </CDBCardBody>
            </CDBCard>
          </CDBContainer>
        </>
        {/* Modal add */}
        <>
          <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
          >
            <Modal.Header closeButton>
              <Modal.Title>New</Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmit}>
              <Modal.Body>
                <Row>
                  <Col>
                    <Form.Group controlId="titlename">
                      <Form.Label>Title Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter Title Name"
                        value={titlename}
                        onChange={(e) => setTitlename(e.target.value)}
                        required
                      />
                    </Form.Group>
                    <Form.Group controlId="authorname">
                      <Form.Label>Abstract</Form.Label>
                      <textarea
                        className="form-control"
                        id="exampleFormControlTextarea1"
                        rows="3"
                        placeholder="Enter Abstract"
                        value={abstractname}
                        onChange={(e) => setAuthorname(e.target.value)}
                        required
                      />
                    </Form.Group>
                    <Form.Group controlId="booknumbercode">
                      <Form.Label>Book Number/Code</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter book number/code"
                        value={booknumbercode}
                        onChange={(e) => setBooknumbercode(e.target.value)}
                        required
                      />
                    </Form.Group>
                    <Form.Group controlId="ISBNNumber">
                      <Form.Label>ISBN Number</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter ISBN number"
                        value={ISBNNumber}
                        onChange={(e) => setISBNNumber(e.target.value)}
                        required
                      />
                    </Form.Group>
              
                   

                    {/* <Form.Group controlId="publishername">
                      <Form.Label>Publisher Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter publisher name"
                        value={publishername}
                        onChange={(e) => setPublishername(e.target.value)}
                        required
                      />
                    </Form.Group> */}
 <Form.Group controlId="publishername">
      <Form.Label>Publisher Name</Form.Label>
      <div className="input-group mb-3">
        <Form.Control
          type="text"
          className="form-control"
          placeholder="Enter publisher name"
          value={publishername}
          onChange={(e) => setPublishername(e.target.value)}
          required
        />
        <button
          className="btn btn-outline-secondary dropdown-toggle"
          type="button"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          Dropdown
        </button>
        <ul className="dropdown-menu dropdown-menu-end">
          <li>
            <button
              className="dropdown-item"
              onClick={() => handleDropdownSelect('Publisher A')}
            >
              Publisher A
            </button>
          </li>
          <li>
            <button
              className="dropdown-item"
              onClick={() => handleDropdownSelect('Publisher B')}
            >
              Publisher B
            </button>
          </li>
          {/* Add more dropdown items as needed */}
        </ul>
      </div>
    </Form.Group>








                    <Form.Group controlId="publisheddate">
                      <Form.Label>Published Date</Form.Label>
                      <Form.Control
                        type="date"
                        value={publisheddate}
                        onChange={(e) => setPublisheddate(e.target.value)}
                        required
                      />
                    </Form.Group>



                    
                    <Form.Group controlId="quantity">
                      <Form.Label>Quantity</Form.Label>
                      <Form.Control
                        type="number"
                        placeholder="Enter quantity"
                        value={quantity}
                        onChange={(e) => setQuantity(e.target.value)}
                        required
                      />
                    </Form.Group>
                  </Col>
                </Row>
                {/* Rest of the form fields */}
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                  Close
                </Button>
                <Button variant="primary" type="submit">
                  Save
                </Button>
              </Modal.Footer>
            </Form>
          </Modal>
        </>

        {/* End modal add */}

        {/* Modal edit */}
        <>{/* Contents of edit modal */}</>
        {/* End modal edit */}
      </CDBBox>
    </div>
  );
}
