import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { CDBBox, CDBContainer } from "cdbreact";
import { Link } from "react-router-dom";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import { NavLink } from "react-router-dom";
import AdminDashboard from "../adminside/AdminDashboard";
import SidebarLayout from "./SidebarLayout";
import React, { useState } from 'react';
import { Form, Dropdown  } from 'react-bootstrap';
import { SelectList } from 'react-native-dropdown-select-list'

export default function Returnedbooks({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  // const [selectedPublishers, setSelectedPublishers] = useState([]);

  // const handleDropdownSelect = (value, color) => {
  //   if (!selectedPublishers.some(publisher => publisher.value === value)) {
  //     setSelectedPublishers([...selectedPublishers, { value, color }]);
  //     updateInputValue([...selectedPublishers, { value, color }]);
  //   }
  // };

  // const updateInputValue = (publishers) => {
  //   const input = document.getElementById('publisherInput');
  //   if (input) {
  //     input.value = publishers.map(publisher => publisher.value).join(', ');
  //   }
  // };
  // const [selected, setSelected] = React.useState("");
  
  const data = [
      {key:'1', value:'Mobiles', disabled:true},
      {key:'2', value:'Appliances'},
      {key:'3', value:'Cameras'},
      {key:'4', value:'Computers', disabled:true},
      {key:'5', value:'Vegetables'},
      {key:'6', value:'Diary Products'},
      {key:'7', value:'Drinks'},
  ]

  return (
    <div
    style={{ display: "flex", height: "100vh", overflow: "scroll initial" }}
  >
    <CDBBox display="flex" alignContent="start">
      <CDBSidebar textColor="#fff" backgroundColor="#800000">
        <SidebarLayout />
      </CDBSidebar>
    </CDBBox>

    <CDBBox
      display="flex"
      justifyContent="start"
      style={{
        width: "100%",
        height: "100%",
      }}
    >
      {/* put this a screen*/}
      {/* <SelectList 
        setSelected={(val) => setSelected(val)} 
        data={data} 
        save="value"
    /> */}
          
    </CDBBox>
  </div>
  );
}
