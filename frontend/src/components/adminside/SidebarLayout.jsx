import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { CDBBox, CDBContainer } from "cdbreact";
import { Link } from "react-router-dom";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import { NavLink } from "react-router-dom";
import AdminDashboard from "../adminside/AdminDashboard";
import React, { useState } from 'react';


export default function SidebarLayout({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };
  
  const [showLibraries, setShowLibraries] = useState(false);

  return (
    <>
        
          <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large"></i>}>
            
          </CDBSidebarHeader>

          <CDBSidebarContent className="sidebar-content">
            <CDBSidebarMenu>
              <NavLink
                exact
                to="/admin/dashboard"
                activeClassName="activeClicked"
              >
                <CDBSidebarMenuItem icon="columns">
                  Dashboard
                </CDBSidebarMenuItem>
              </NavLink>
             
             
             <CDBSidebarMenuItem
                icon="book"
                onClick={() => setShowLibraries(!showLibraries)}
              >
                Library
                {showLibraries && (
                  <CDBSidebarMenuItem>
                    <NavLink exact to="/admin/books" activeClassName="activeClicked">
                      <CDBSidebarMenuItem>Library 1</CDBSidebarMenuItem>
                    </NavLink>
                    <NavLink exact to="/admin/library2" activeClassName="activeClicked">
                      <CDBSidebarMenuItem>Library 2</CDBSidebarMenuItem>
                    </NavLink>
                  </CDBSidebarMenuItem>
                  
                )}
                

              </CDBSidebarMenuItem>


              <NavLink
                exact
                to="/admin/borrowedbooks"
                activeClassName="activeClicked"
              >
                <CDBSidebarMenuItem icon="book">
                  <Link to="/admin/borrowedbooks">Users</Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink
                exact
                to="/admin/returnedbooks"
                activeClassName="activeClicked"
              >
                <CDBSidebarMenuItem icon="book">
                  <Link to="/admin/returnedbooks">Catalog</Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/admin/damagecharge" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="address-book">
                  <Link to="/admin/damagecharge">Catalog 2</Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/admin/students" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="user"><Link to="/admin/students">Catalog 3</Link></CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/admin/admins" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="user"><Link to="/admin/admins">Catalog 4</Link></CDBSidebarMenuItem>
              </NavLink>
         
              <NavLink
                exact
                to="/hero404"
                target="_blank"
                activeClassName="activeClicked"
              >
                <CDBSidebarMenuItem icon="arrow-right">
                  <Button variant="primary" onClick={logOut}>
                    Log out
                  </Button>
                </CDBSidebarMenuItem>
              </NavLink>
            </CDBSidebarMenu>
          </CDBSidebarContent>
          <CDBContainer></CDBContainer>

          <CDBSidebarFooter style={{ textAlign: "center" }}>
            <div
              style={{
                padding: "20px 5px",
              }}
            >
            
            </div>
          </CDBSidebarFooter>
        
     


    </>
  );
}
