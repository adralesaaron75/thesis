import React, { useState, useEffect } from "react";
import { Table, Button, Modal, Row, Col, Form } from "react-bootstrap";
import { toast } from "react-toastify";
import { VITE_BACKEND_URL } from "../../../App";
import SidebarLayout from "../SidebarLayout";

export default function TbleData ({ userType }) {
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [booksPerPage] = useState(15);

  useEffect(() => {
    getAllBooks();
  }, []);

  const getAllBooks = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        setData(data.data);
      });
  };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const indexOfLastBook = currentPage * booksPerPage;
  const indexOfFirstBook = indexOfLastBook - booksPerPage;
  const currentBooks = data.slice(indexOfFirstBook, indexOfLastBook);



  const truncateAuthorName = (author) => {
    if (author.length > 30) {
      return author.substring(0, 30) + "...";
    }
    return author;
  };

  // Convert to base64 format
  function covertToBase64(e) {
    console.log(e);
    var reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = () => {
      console.log(reader.result); // Base64encoded string
      // setImage(reader.result); // Excluded based on your request
    };
    reader.onerror = (error) => {
      console.log("Error: ", error);
    };
  }
  // End convert to base64 format

  // Add book
  const handleSubmit = async (e) => {
    if (userType === "admin") {
      e.preventDefault();
      alert("You are not Admin");
    } else {
      e.preventDefault();
      await fetch(`${VITE_BACKEND_URL}/api/add-book`, {
        method: "POST",
        crossDomain: true,
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          titlename,
          booknumbercode,
          ISBNNumber,
          abstractname,
          publishername,
          publisheddate,
          quantity,
          // base64: image, // Excluded based on your request
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data, "Admin-added");
          if (data.status === "ok") {
            toast.success("Add book successfully");
            setTimeout(() => {
              window.location.href = "/admin/books";
            }, 2000);
            getAllBooks();
          } else {
            toast.error("Something went wrong");
          }
        });
    }
  };
  // End add book

  // Add modal
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  // End add modal

  // Edit modal
  const [showedit, setShowedit] = useState(false);
  const handleCloseedit = () => setShowedit(false);
  const handleShowedit = () => setShowedit(true);
  // End edit modal

  return (
    <div style={{ display: "flex", height: "100vh", overflow: "scroll initial" }}>
      <div style={{ marginLeft: "20px" }}>
        <h2>Books</h2>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Book Name</th>
              <th>Book Number/Code</th>
              <th>ISBN Number</th>
              <th>Abstract</th>
              <th>Publisher Name</th>
              <th>Published Date</th>
              <th>Quantity</th>
            </tr>
          </thead>
          <tbody>
            {currentBooks.map((book, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{book.titlename}</td>
                <td>{book.booknumbercode}</td>
                <td>{book.ISBNNumber}</td>
                <td>{truncateAuthorName(book.abstractname)}</td>
                <td>{book.publishername}</td>
                <td>{book.publisheddate}</td>
                <td>{book.quantity}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <div className="pagination">
          <Button
            variant="outline-secondary"
            onClick={() => handlePageChange(currentPage - 1)}
            disabled={currentPage === 1}
          >
            Previous
          </Button>
          <span style={{ margin: "0 10px" }}>Page {currentPage}</span>
          <Button
            variant="outline-secondary"
            onClick={() => handlePageChange(currentPage + 1)}
            disabled={indexOfLastBook >= data.length}
          >
            Next
          </Button>
        </div>
      </div>
    </div>
  );
}
