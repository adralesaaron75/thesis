// Dashboard.jsx

import React from "react";
import ChartComponent from "./Chart";
import { Link } from "react-router-dom";

const AdminDashboard = () => {
  return (
    <div className="dashboard p-1" style={{ width: "100%" }}>
      <div className="sidebar">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" href="#">
              Users
            </a>
          </li>
          <li class="nav-item">
            <Link className="nav-link" to="/admin/admins">
              Admin
            </Link>
          </li>
          <li class="nav-item">
            <Link className="nav-link" to="/admin/admins">
              Student
            </Link>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">
              Disabled
            </a>
          </li>
        </ul>
      </div>
      <div className="main-content p-4">
        <div
          className="charts-container"
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <div
            className="last-orders-chart"
            style={{ width: "35%", marginRight: "15px" }}
          >
            <h2>Last Registered</h2>
            <table className="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Year</th>
                  <th>Course</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Tee Bowman</td>
                  <td>1</td>
                  <td>BSCS</td>
                </tr>
                <tr>
                  <td>Marcos E Velazquez</td>
                  <td>2</td>
                  <td>BSIT</td>
                </tr>
                <tr>
                  <td>Kent Priel</td>
                  <td>1</td>
                  <td>BSCS</td>
                </tr>
                <tr>
                  <td>Steve Shotwell</td>
                  <td>4</td>
                  <td>BSIS</td>
                </tr>
                <tr>
                  <td>Alex Hays</td>
                  <td>1</td>
                  <td>BSIS</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div
            className="chart-container"
            style={{ width: "65%", height: "100%" }}
          >
            <ChartComponent />
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminDashboard;
