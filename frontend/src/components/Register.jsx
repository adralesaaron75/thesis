import Container from "react-bootstrap/Container";
import Carousel from "react-bootstrap/Carousel";
import img1 from "./assets/images/home-library-j0f5epsruiuypf5g.webp";
import img2 from "./assets/images/home-library-uxfooi9id0mxvzdq.webp";
import img3 from "./assets/images/home-library-ey7wp5pfmhiy7hof.webp";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Link } from "react-router-dom";
import { useFormik } from "formik";
import { signupValidation } from "./SignupValidation";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { toast } from 'react-toastify'
import { useState } from "react";
import { VITE_BACKEND_URL } from '../App'
import FormContainer from "./FormContainer";
import logo from "./assets/images/logossu.png";

const initialValues = {
  // studentid: "",
  name: "",
  course: "",
  yearandsection: "",
  email: "",
  password: "",
  confirmpassword: "",
};

function Register() {
  const navigate = useNavigate();
  const[isLoading, setIsLoading] = useState(false)
  const { values, handleBlur, handleChange, handleSubmit, errors } = useFormik({
    initialValues: initialValues,
    validationSchema: signupValidation,  
    onSubmit: async(values) => {
      console.log(values);
      
      try {
        setIsLoading(true);
        const response = await axios.post(`${VITE_BACKEND_URL}/api/register`,values)
        toast.success(`Save ${response.data.name} successfully`)
        setIsLoading(false);
        navigate("/")
      } catch (error) {
        toast.error(error.message)
        // console.log(error)
        setIsLoading(false);
      }
      // console.log('Recieved values of form', values);

    },
  });
  // console.log(formik);

  
  return (
    <FormContainer>
            <div style={{ position: 'relative' }}>
                 <img src={logo} alt="Logo" width={120} height={120} style={{ filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.99))', position: 'absolute', top: '-90px', left: '50%', transform: 'translateX(-50%)' }} />
            </div>



            <h1 style={{ textAlign: 'center', marginTop: '50px', fontSize: '2.5em', color: '#333', fontWeight: 'bold', textShadow: '2px 2px 4px rgba(0, 0, 0, 0.2)' }}>Register</h1>

             {/* Horizontal Bar */}

            <hr style={{ width: '90%', borderTop: '2px solid #ccc', margin: '10px auto', marginBottom: '30px'}} />
        
          <Card style={{filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}>
           
            <Card.Body>
              {/* <Card.Title>Special title treatment</Card.Title> */}
              <Form onSubmit={handleSubmit}>
                {/* <Form.Group className="mb-1" controlId="formBasicStudID">
                  <Form.Label style={{ fontSize: '0.9rem', marginBottom: '0.3rem', textAlign: 'left', width: '85%' }}>Student ID</Form.Label>
                  <Form.Control
                    value={values.studentid}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="text"
                    name="studentid"
                    placeholder="Enter Student ID Number"
                    autoComplete="off"
                    style={{ fontSize: '0.8rem',  padding: '0.3rem', paddingLeft: '0.9rem', width: '100%', borderRadius: '7px', filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}
                  />
                  <Form.Text className="text-muted">
                    {errors.studentid && <small>{errors.studentid}</small>}
                  </Form.Text>
                </Form.Group> */}

                <Form.Group className="mb-1" controlId="formBasicName">
                  <Form.Label style={{ fontSize: '0.9rem', marginBottom: '0.3rem', textAlign: 'left', width: '85%' }}>User Name</Form.Label>
                  <Form.Control
                    value={values.name}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="text"
                    name="name"
                    placeholder="Enter Name"
                    autoComplete="off"
                    style={{ fontSize: '0.99rem',  padding: '0.3rem', paddingLeft: '0.9rem', width: '100%', borderRadius: '7px', filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}
                  />
                  <Form.Text className="text-muted">
                    {errors.name && <small>{errors.name}</small>}
                  </Form.Text>
                </Form.Group>

                <Form.Group className="mb-1" controlId="formBasicCourse">
                  <Form.Label style={{ fontSize: '0.9rem', marginBottom: '0.3rem', textAlign: 'left', width: '85%' }}>Course</Form.Label>
                  <Form.Control
                    value={values.course}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="text"
                    name="course"
                    placeholder="Enter Course Name"
                    autoComplete="off"
                    style={{ fontSize: '0.99rem',  padding: '0.3rem', paddingLeft: '0.9rem', width: '100%', borderRadius: '7px', filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}
                  />
                  <Form.Text className="text-muted">
                    {errors.course && <small>{errors.course}</small>}
                  </Form.Text>
                </Form.Group>

                <Form.Group
                  className="mb-1"
                  controlId="formBasicYearandSection"
                >
                  <Form.Label style={{ fontSize: '0.9rem', marginBottom: '0.3rem', textAlign: 'left', width: '85%' }}>Year & Section</Form.Label>
                  <Form.Control
                    value={values.yearandsection}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="text"
                    name="yearandsection"
                    placeholder="Enter Year and Section"
                    autoComplete="off"
                    style={{ fontSize: '0.99rem',  padding: '0.3rem', paddingLeft: '0.9rem', width: '100%', borderRadius: '7px', filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}
                  />
                  <Form.Text className="text-muted">
                    {errors.yearandsection && (
                      <small>{errors.yearandsection}</small>
                    )}
                  </Form.Text>
                </Form.Group>

                <Form.Group className="mb-1" controlId="formBasicEmail">
                  <Form.Label style={{ fontSize: '0.9rem', marginBottom: '0.3rem', textAlign: 'left', width: '85%' }}> Email address</Form.Label>
                  <Form.Control
                    value={values.email}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="email"
                    name="email"
                    placeholder="Enter email"
                    autoComplete="off"
                    style={{ fontSize: '0.99rem',  padding: '0.3rem', paddingLeft: '0.9rem', width: '100%', borderRadius: '7px', filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}
                  />
                  <Form.Text className="text-muted">
                    {errors.email && <small>{errors.email}</small>}
                  </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label style={{ fontSize: '0.9rem', marginBottom: '0.3rem', textAlign: 'left', width: '85%' }}>Password</Form.Label>
                  <Form.Control
                    value={values.password}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="password"
                    name="password"
                    placeholder="Password"
                    autoComplete="off"
                    style={{ fontSize: '0.99rem',  padding: '0.3rem', paddingLeft: '0.9rem', width: '100%', borderRadius: '7px', filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}
                  />
                  <Form.Text className="text-muted">
                    {errors.password && <small>{errors.password}</small>}
                  </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicCPassword">
                  <Form.Label style={{ fontSize: '0.9rem', marginBottom: '0.3rem', textAlign: 'left', width: '85%' }}>Confirm Password</Form.Label>
                  <Form.Control
                    value={values.confirmpassword}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="password"
                    name="confirmpassword"
                    placeholder="Confirm Password"
                    autoComplete="off"
                    style={{ fontSize: '0.99rem',  padding: '0.3rem', paddingLeft: '0.9rem', width: '100%', borderRadius: '7px', filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}
                  />
                  <Form.Text className="text-muted">
                    {errors.confirmpassword && (
                      <small>{errors.confirmpassword}</small>
                    )}
                  </Form.Text>
                </Form.Group>

                <Button variant="success" type="submit" className="w-100" style={{ backgroundColor: 'maroon', color: 'white', float: 'right' }}>
                  Sign Up
                </Button>
              </Form>
              <Form.Group className="mb-3 mt-2" controlId="formBasicCheckbox">
                Already have an account ? <Link to="/login">Login</Link>
              </Form.Group>
            </Card.Body>
          </Card>
      
    </FormContainer>
  );
}

export default Register;
