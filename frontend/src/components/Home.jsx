import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Link } from "react-router-dom";
import logo from "./assets/images/logossu.png";
import { toast } from 'react-toastify'
import { VITE_BACKEND_URL } from '../App'
import { useState } from "react";
import FormContainer from "./FormContainer";

function Home() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async(e) => {
    e.preventDefault();
    console.log(email, password);
    await fetch(`${VITE_BACKEND_URL}/api/login`, {
      method: "POST",
      crossDomain: true,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.status === "ok") {
          toast.success(`Login successfully`)
          window.localStorage.setItem("token", data.data);
          window.localStorage.setItem("loggedIn", true);
          setTimeout(() => {
            window.location.href = "./dashboard/dashboard";
          }, 2000);
        } else if (data.status === "notlogin") {
          toast.warning(`Login Failed`)
          setTimeout(() => {
            window.location.href = "/";
          }, 2000);
        }
        if (data.status === "error") {
          toast.error("Login failed")
        }
      });
  }

  return (
    <FormContainer>
              <div style={{ position: 'relative' }}>
                 <img src={logo} alt="Logo" width={120} height={120} style={{ filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.99))', position: 'absolute', top: '-90px', left: '50%', transform: 'translateX(-50%)' }} />
            </div>



            <h1 style={{ textAlign: 'center', marginTop: '50px', fontSize: '2.5em', color: '#333', fontWeight: 'bold', textShadow: '2px 2px 4px rgba(0, 0, 0, 0.2)' }}>Login</h1>

             {/* Horizontal Bar */}

            <hr style={{ width: '90%', borderTop: '2px solid #ccc', margin: '10px auto', marginBottom: '30px'}} />
     
          
            <Card style={{filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}> 
              <Card.Body>
                <Form onSubmit={handleSubmit}>
                  <Form.Group className="mb-1" controlId="formBasicEmail">
                    <Form.Label style={{ fontSize: '0.9rem', marginBottom: '0.3rem', textAlign: 'left', width: '85%' }}> Email address</Form.Label>
                    <Form.Control
                      onChange={(e) => setEmail(e.target.value)}
                      type="email"
                      name="email"
                      placeholder="email"
                      autoComplete="off"
                      style={{ fontSize: '0.99rem',  padding: '0.3rem', paddingLeft: '0.9rem', width: '100%', borderRadius: '7px', filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label style={{ fontSize: '0.9rem', marginBottom: '0.3rem', textAlign: 'left', width: '85%' }}>Password</Form.Label>
                    <Form.Control
                      onChange={(e) => setPassword(e.target.value)}
                      type="password"
                      name="password"
                      placeholder="password"
                      autoComplete="off"
                      style={{ fontSize: '0.99rem',  padding: '0.3rem', paddingLeft: '0.9rem', width: '100%', borderRadius: '7px', filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.1))'}}
                    />
                  </Form.Group>

                  <Button variant="success" type="submit" className="w-100" style={{ backgroundColor: 'maroon', color: 'white', float: 'right' }}>
                    Sign In
                  </Button>
                </Form>
                <Form.Group className="mb-3 mt-2" controlId="formBasicCheckbox">
                  Don't have an account ? <Link to="/register">Register</Link>
                </Form.Group>
                <Form.Group className="mb-3 mt-2" controlId="formBasicCheckbox">
                  Search ? <Link to="/searchhome">go to home</Link>
                </Form.Group>
              </Card.Body>
            </Card>
        
       
    
    </FormContainer>
  );
}

export default Home;
