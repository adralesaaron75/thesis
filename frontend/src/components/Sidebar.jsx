import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import logo from "./assets/images/2072841.png";
import { CDBBox, CDBContainer } from "cdbreact";
import { Link } from "react-router-dom";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import { NavLink } from "react-router-dom";
import AdminDashboard from "./adminside/AdminDashboard";

export default function NAME_OF_FUNCTION({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  return (
    <div
      style={{ display: "flex", height: "100vh", overflow: "scroll initial" }}
    >
      <CDBBox display="flex" alignContent="start">
        <CDBSidebar textColor="#fff" backgroundColor="#800000">
          <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large"></i>}>
            
          </CDBSidebarHeader>

          <CDBSidebarContent className="sidebar-content">
            <CDBSidebarMenu>
              <NavLink
                exact
                to="/admin/dashboard"
                activeClassName="activeClicked"
              >
                <CDBSidebarMenuItem icon="columns">
                  Dashboard
                </CDBSidebarMenuItem>
              </NavLink>

              <NavLink exact to="/admin/books" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="book">
                  <Link to="/admin/books">Library</Link>
                </CDBSidebarMenuItem>
              </NavLink>
              
              <NavLink
                exact
                to="/admin/borrowedbooks"
                activeClassName="activeClicked"
              >
                <CDBSidebarMenuItem icon="book">
                  <Link to="/admin/borrowedbooks">Users</Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink
                exact
                to="/admin/returnedbooks"
                activeClassName="activeClicked"
              >
                <CDBSidebarMenuItem icon="book">
                  <Link to="/admin/returnedbooks">Catalog 1</Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/admin/damagecharge" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="address-book">
                  <Link to="/admin/damagecharge">Catalog 2</Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/admin/students" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="user"><Link to="/admin/students">Catalog 3</Link></CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/admin/admins" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="user"><Link to="/admin/admins">Catalog 4</Link></CDBSidebarMenuItem>
              </NavLink>
         
              <NavLink
                exact
                to="/hero404"
                target="_blank"
                activeClassName="activeClicked"
              >
                <CDBSidebarMenuItem icon="arrow-right">
                  <Button variant="primary" onClick={logOut}>
                    Log out
                  </Button>
                </CDBSidebarMenuItem>
              </NavLink>
            </CDBSidebarMenu>
          </CDBSidebarContent>
          <CDBContainer></CDBContainer>

          <CDBSidebarFooter style={{ textAlign: "center" }}>
            <div
              style={{
                padding: "20px 5px",
              }}
            >
            
            </div>
          </CDBSidebarFooter>
        </CDBSidebar>
      </CDBBox>



      <CDBBox
        
        display="flex"
        justifyContent="start"
        style={{
          width: "100%",
          height: "100%",
        }}
      >


       {/* put this a  screen*/}
         

      </CDBBox>
    </div>
  );
}
