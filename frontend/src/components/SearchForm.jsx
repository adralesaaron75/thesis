import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, InputGroup } from 'react-bootstrap';
import { FaSearch } from 'react-icons/fa';

const searchData = [
  { "full_name": "Farrel Hoggin" },
  { "full_name": "Irma Olech" },
  { "full_name": "Emmit Gallacher" },
  { "full_name": "Dunn Astlet" },
  { "full_name": "Burg Peaddie" },
  { "full_name": "aMolli Knoller" },
  { "full_name": "Ellen Cheak" },
  { "full_name": "Kaela Hannibal" },
  { "full_name": "Roxanna Mughal" },
  { "full_name": "Sharona Robberts" },
  { "full_name": "Tybiea Dudding" },
];

const SearchForm = () => {
  const [searchType, setSearchType] = useState("option1");
  const [searchInput, setSearchInput] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const handleSearchInputChange = (e) => {
    setSearchInput(e.target.value);
  }

  useEffect(() => {
    if (!searchInput.trim()) {
      setSearchResults([]);
      return;
    }

    const filteredResults = searchData.filter(item => {
      return item.full_name.toLowerCase().includes(searchInput.toLowerCase());
    });

    const limitedResults = filteredResults.slice(0, 10).map(item => {
      const regex = new RegExp(`(${searchInput})`, 'gi');
      const highlightedName = item.full_name.replace(regex, '<span style="background-color: yellow">$1</span>');
      return { ...item, highlightedName };
    });

    setSearchResults(limitedResults);
  }, [searchInput]);

  const handleSuggestionClick = (fullName) => {
    setSearchInput(fullName);
    setSearchResults([]);
  }

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      handleSearch();
    }
  }

  return (
    <Container style={{ position: "relative" }}>
      <Row>
        <Col>
          <Form.Group>
            <InputGroup className="mb-3" style={{ width: "50vw", marginTop: "10px" }}>
              <InputGroup.Text id="basic-addon1" style={{ borderRadius: "20px 0 0 20px", background: "#f0f0f0", border: "none" }}>
                <FaSearch />
              </InputGroup.Text>
              <Form.Control
                placeholder="Search"
                aria-label="Search"
                aria-describedby="basic-addon1"
                value={searchInput}
                onChange={handleSearchInputChange}
                onKeyPress={handleKeyPress}
                style={{ borderRadius: "0 20px 20px 0", border: "1px solid #ccc" }}
              />
              
              <Form.Group style={{position: 'absolute', left: 0, top: "calc(100% + 0px)", marginLeft:'40px',width: "92%", background: "white", zIndex: 2, color: 'black'}}>
                {searchResults.length > 0 && (
                  <ul style={{ padding: 0 }}>
                    {searchResults.map((item, index) => (
                      <li key={index} style={{padding:'5px', color: "black", listStyleType: "none", cursor: "pointer" }} onClick={() => handleSuggestionClick(item.full_name)}>
                        <span dangerouslySetInnerHTML={{ __html: item.highlightedName }} />
                      </li>
                    ))}
                  </ul>
                )}
              </Form.Group>

            </InputGroup>

            <Form.Group style={{ display: "flex", justifyContent: "center", width: "45vw", marginTop: "10px", zIndex: 3 }}>
              <Form.Check
                type="radio"
                id="option1"
                label="Option 1"
                value="option1"
                checked={searchType === "option1"}
                onChange={() => setSearchType("option1")}
                style={{ marginRight: "50px", fontFamily: "Arial", fontSize: "14px", color: "black" }}
              />
              <Form.Check
                type="radio"
                id="option2"
                label="Option 2"
                value="option2"
                checked={searchType === "option2"}
                onChange={() => setSearchType("option2")}
                style={{ fontFamily: "Arial", fontSize: "14px", color: "black" }}
              />
            </Form.Group>
          </Form.Group>
        </Col>
      </Row>
    </Container>
  );
}

export default SearchForm;
