import React from 'react';
import ReactDOM from 'react-dom';

const SearchResultCount = ({ currentPage, itemsPerPage, totalItems }) => {
  const startIndex = (currentPage - 1) * itemsPerPage + 1;
  const endIndex = Math.min(currentPage * itemsPerPage, totalItems);

  return (
    <div>
      Showing {startIndex} - {endIndex} out of {totalItems}
    </div>
  );
};

const App = () => {
  return (
    <div>
      <h1>Search Results</h1>
      <SearchResultCount currentPage={1} itemsPerPage={10} totalItems={234} />
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));

export default SearchResultCount
