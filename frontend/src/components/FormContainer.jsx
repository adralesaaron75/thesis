import { Container, Row, Col } from "react-bootstrap"

const FormContainer = ({children}) => {
    return (
        <Container style={{ filter: 'drop-shadow(0 15px 14px rgba(128, 0, 0, 0.5))'  }}>
            <Row className="justify-content-md-center mt-5">{/*style={{ filter: 'drop-shadow(0 4px 8px rgba(255, 0, 0, 0.99))'  }}*/}
                <Col xs={12} md={4} className="card p-5">
                {children}
                </Col>

            </Row>
        </Container>
    )
}

export default FormContainer