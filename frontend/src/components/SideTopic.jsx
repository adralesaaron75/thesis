import React from "react";
import { Row, Col } from "react-bootstrap";

const SideTopic = () => {
  return (
    <>
      <div class="p-2"  style={{ width: "80%" }}>
        <ul>
          <label
            style={{
              fontFamily: "Arial",
              fontSize: "18px",
              fontWeight: "bold",
              color: "#333",
            }}
          >
            Topic
          </label>
          <hr
            class="d-flex justify-content-start"
            style={{
              width: "auto", // Set width to auto to allow it to start from the left
              borderTop: "1px solid #000000",
              margin: "0",
              marginTop: "10px",
              marginBottom: "10px",
            }}
          />
         
          <li className="list-group-item d-flex justify-content-between align-items-center">
            Category<span>14</span>
          </li>
          <li className="list-group-item d-flex justify-content-between align-items-center">
            Category<span>2</span>
          </li>
          <li className="list-group-item d-flex justify-content-between align-items-center">
            Category<span>1</span>
          </li>
          
          </ul>
     
      </div>
      <div class="p-2"  style={{ width: "80%" }}>
        <ul>
          <label
            style={{
              fontFamily: "Arial",
              fontSize: "18px",
              fontWeight: "bold",
              color: "#333",
            }}
          >
            Course
          </label>
          <hr
            class="d-flex justify-content-start"
            style={{
              width: "auto", // Set width to auto to allow it to start from the left
              borderTop: "1px solid #000000",
              margin: "0",
              marginTop: "10px",
              marginBottom: "10px",
            }}
          />
         
          <li className="list-group-item d-flex justify-content-between align-items-center">
            BSCS<span>2</span>
          </li>
          <li className="list-group-item d-flex justify-content-between align-items-center">
          BSIS<span>3</span>
          </li>
          <li className="list-group-item d-flex justify-content-between align-items-center">
          BSIT<span>1</span>
          </li>
          
          </ul>
     
      </div>
    </>
  );
};

export default SideTopic;
