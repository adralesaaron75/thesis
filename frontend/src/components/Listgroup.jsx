import React from "react";
import { CDBListGroup, CDBListGroupItem, CDBBadge, CDBContainer } from "cdbreact";
import { Col } from "react-bootstrap";

const LIstGroup = () => {
  return (
            <CDBContainer style={{ width: "30rem" }}>
              <CDBListGroupItem className="d-flex justify-content-between align-items-center">
                List Group <Col>19</Col>
              </CDBListGroupItem>
              <CDBListGroupItem className="d-flex justify-content-between align-items-center">
                List Group
                <CDBBadge>19</CDBBadge>
              </CDBListGroupItem>
              <CDBListGroupItem className="d-flex justify-content-between align-items-center">
                List Group
                <CDBBadge color="success">19</CDBBadge>
              </CDBListGroupItem>
              <CDBListGroupItem className="d-flex justify-content-between align-items-center">
                List Group
                <CDBBadge color="danger">19</CDBBadge>
              </CDBListGroupItem>
              <CDBListGroupItem className="d-flex justify-content-between align-items-center">
                List Group<CDBBadge color="info">19</CDBBadge>
              </CDBListGroupItem>
              <CDBListGroupItem className="d-flex justify-content-between align-items-center">
                List Group<CDBBadge>19</CDBBadge>
              </CDBListGroupItem>
            </CDBContainer>
  );
};
export default  LIstGroup;