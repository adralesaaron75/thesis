import React from "react";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { FaBars, FaSignInAlt, FaSignOutAlt } from "react-icons/fa";

function NavbarMenu() {
  const isUserSignedIn = !!localStorage.getItem("token");

  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

 
  let navLinks;
  if (isUserSignedIn) {
    navLinks = (
      


<Nav className="ms-auto">
       <Nav.Link className="navitem">
        <Link to="/" className="nav-link">Home</Link>
      </Nav.Link>
      <Nav.Link className="navitem">
        <Link to="/aboutus" className="nav-link">About Us</Link>
      </Nav.Link>
      <Nav.Link className="navitem">
         <Link onClick={logOut} to="/login" className="nav-link" ><FaSignOutAlt/> Logout</Link>
       </Nav.Link>
     </Nav>
    );
  } else {
    navLinks = (
      <Nav className="ms-auto">
     
      {/* Add more navigation links as needed */}


      <Nav.Link className="navitem">
         <Link to="/login" className="nav-link"><FaSignInAlt/> Login</Link>
       </Nav.Link>
    </Nav>

    
       
    );
  }

  return (
    <Navbar style={{ background: "#800000", minHeight: "2rem" }} variant="dark" expand="lg">
      <Container fluid>
        <Link to="/" className="navbar-brand">
          {/* Your logo */}
        </Link>
        <Navbar.Toggle aria-controls="navbarScroll">
          <FaBars style={{ color: "#fff", fontSize: "1.5rem" }} />
        </Navbar.Toggle>
        <Navbar.Collapse id="navbarScroll">
          {navLinks}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavbarMenu;
