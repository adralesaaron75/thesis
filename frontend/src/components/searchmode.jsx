import React from "react";
import { useState } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { FaHeart } from "react-icons/fa";
import StarRating from "./StarRating";
import ThesisCard from "./ThesisCard";
import Footer from "./Footer"; // Import the Footer component
import InputGroup from "react-bootstrap/InputGroup";
import { FaSearch } from "react-icons/fa";
import SearchForm from "./SearchForm";
import SideTopic from "./SideTopic";
import SearchResultCount from "./ResultCounter";
import logo from "./assets/images/logossu.png";

function SearchBar() {
  
  return (
    <div>
      
      <div style={{ background: "maroon", width: "100%", height: "30vh" }}>
        {" "}
       
        <SearchForm/>
            <div style={{ position: 'relative' }}>
                 <img src={logo} alt="Logo" width={170} height={170} style={{ filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.99))', position: 'absolute', top: '-70px', left: '87%', transform: 'translateX(-50%)' }} />
            </div>
        
      </div>

      <Row style={{ marginTop: "20px" }}>
        <Col style={{ marginLeft: "auto" }}>
        
          <SideTopic/>
        </Col>

        <Col style={{ marginRight: "3%" }}>
            <div className="d-flex justify-content-end"><SearchResultCount
              currentPage={1}
              itemsPerPage={3}
              totalItems={3}
              
          />
          </div>
          <ThesisCard
            date="2024"
            title="Title 1Title 1"
            // content="Custom content for Thesis2"
            categories={["BSIT2", "BSIT2", "BSIT-3"]}
          />
          <ThesisCard
            date="2023"
            title="Title 2"

            categories={["BSCS1"]}
          />
          <ThesisCard
            date="2023"
            title="Title 3"
            // content
            categories={["BSCS1"]}
          />
          
        </Col>
      </Row>


      


      

      {/* Include the Footer component */}
      <Footer />
    </div>
  );
}

export default SearchBar;




